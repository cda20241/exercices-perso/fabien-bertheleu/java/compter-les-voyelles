package org.example;
import org.apache.commons.lang3.StringUtils;
import java.text.Normalizer;
import java.util.Locale;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {


    //Dans cette partie, je supprime les accents et majuscules
        // Je demande sa phrase à l'utilisateur, je la scanne et en fait un string,
        // le tout en utilisant "import java.util.Scanner"
        Scanner scanner = new Scanner(System.in);
        System.out.print("Entrez une phrase: ");
        String entreeUtilisateur = scanner.nextLine();

        // Je transforme la phrase en minuscule
        String entreeUtilisateurMinuscule = entreeUtilisateur.toLowerCase(Locale.ROOT) ;

        // Je supprime les accents en utilisant "import.java.text.Normalizer"
        String entreeUtilisateurSansAccent = Normalizer.normalize(entreeUtilisateurMinuscule, Normalizer.Form.NFD);


    // Dans cette partie, je cherche et j'imprime les différentes voyelles une par une.
        // j'utilise la collection "org.apache.commons.lang3.StringUtils"

        //je cherche à ressortir et compter les "a" de ma phrase
        int compteA = StringUtils.countMatches(entreeUtilisateurSansAccent, "a");
        System.out.println("La lettre A est présente " + compteA + " fois.");

        //je cherche à ressortir et compter les "e" de ma phrase
        int compteE = StringUtils.countMatches(entreeUtilisateurSansAccent, "e" );
        System.out.println("La lettre E est présente " + compteE + " fois.");

        //je cherche à ressortir et compter les "i" de ma phrase
        int compteI = StringUtils.countMatches(entreeUtilisateurSansAccent, "i");
        System.out.println("La lettre I est présente " + compteI + " fois.");

        //je cherche à ressortir et compter les "o" de ma phrase
        int compteO = StringUtils.countMatches(entreeUtilisateurSansAccent, "o");
        System.out.println("La lettre A est présente " + compteO + " fois.");

        //je cherche à ressortir et compter les "u" de ma phrase
        int compteU = StringUtils.countMatches(entreeUtilisateurSansAccent, "u");
        System.out.println("La lettre A est présente " + compteU + " fois.");

        //je cherche à ressortir et compter les "y" de ma phrase
        int compteY = StringUtils.countMatches(entreeUtilisateurSansAccent, "y");
        System.out.println("La lettre Y est présente " + compteY + " fois.");

    }
}